require 'test_helper'

class UsersLinkTest < ActionDispatch::IntegrationTest

  test "sample app" do
    get "/"
    assert_select "a#logo[href=?]", root_path
    assert_select "a#logo", "sample app"
  end
  
  test "home" do
    get "/"
    assert_select "a#home[href=?]", root_path
    assert_select "a#home", "Home"
  end
  
  test "help" do
    get "/"
    assert_select "a#help[href=?]", help_path
    assert_select "a#help", "Help"
  end
  
  test "log in" do
    get "/"
    assert_select "a#login[href=?]", login_path
    assert_select "a#login", "Log in"
  end
  
  test "Sing up now!" do
    get "/"
    assert_select "a#signupnow[href=?]", signup_path
    assert_select "a#signupnow", "Sign up now!"
  end
  
  test "about" do
    get "/"
    assert_select "a#about[href=?]", about_path
    assert_select "a#about", "About"
  end
  
  test "contact" do
    get "/"
    assert_select "a#contact[href=?]", contact_path
    assert_select "a#contact", "Contact"
  end

  def setup
    @user = users(:michael)
  end

  test "users/1/edit not logged in" do
    get edit_user_path(@user)
    assert_redirected_to login_path
  end
  
  test "users/1/edit logged in" do
    log_in_as(@user, remember_me: '0')
    get edit_user_path(@user)
    assert_template 'users/edit'
  end
end
