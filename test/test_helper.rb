ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
  include ApplicationHelper

  def is_logged_in?
    !session[:user_id].nil?
  end
  
  # テストユーザとしてログインする
  def log_in_as(user)
    session[:user_id] = user.id
  end
  
  #フレンドリーフォワーディン用のリダイレクト先テスト用ログアウト
  def log_out
    session[:user_id] = nil
  end
end

class ActionDispatch::IntegrationTest
  
  # テストユーザとしてログインする
  def log_in_as(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me } }
  end
end
